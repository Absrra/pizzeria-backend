/**
 * PedidoController
 *
 * @description :: Server-side logic for managing pedidoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    
    create: function (req, res){
        
        console.log(req.body);
        if (Object.keys(req.body).length === 0){
            res.json({
                code: 200,
                message: 'No hay data'
            })
        }else{

            Pedido.create(req.body
            ).exec(function (err, data) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'success',
                    content: data.id
                });
            })

        }
        
    },
    find: function (req, res) {
        
        Pedido.find().exec(function (err, users) {
            if(err) return res.json({ err: err }, 500);
            else res.json(users);
        });
    },
    findOne: function (req, res) {
        if (req.params.id === undefined ){
            res.json({
                code: 200,
                message: 'no ah proporcionado un id'
            })
        }else{
            
            Pedido.findOne({ id: req.params.id }).exec(function (err, user) {
                if(err) return res.json({ err: err }, 500);
                else res.json(user);
            });

        }
    },
    destroy: function (req, res) {

        if (req.params.id === undefined) {
            res.json({
                code: 200,
                message: 'no ha proporcionado un id'
            })
        }else{
            Pedido.destroy({id: req.params.id}).exec(function (err, user) {
                if(err) return res.json({ err: err }, 500);
                else res.json({
                    code: 200,
                    message: 'user delete',
                    content: user
                });
            });
        }
    },
    update: function (req, res) {
        if (req.params.id === undefined) {
            res.json({
                code: 200,
                message: 'no ha proporcionado un id'
            })
        }else{
            Pedido.update(
                {id: req.params.id},
                req.body
            ).exec(function (err, data) {
                if(err){ 
                    return res.json({ err: err }, 500);
                }else { 
                    
                    return res.json({
                        code: 200,
                        message: 'Update data',
                        content: data[0]
                    });
                }
            });
        }
        
    }

};

